export default [
  {
    "name": "Luke Skywalker",
    "image": "/images/character-1.png"
  },
  {
    "name": "Darth Vader",
    "image": "/images/character-4.png"
  },
  {
    "name": "Obi-wan Kenobi",
    "image": "/images/character-unknown.png"
  },
  {
    "name": "R2-D2",
    "image": "/images/character-3.png"
  }
]