import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { checkA11y } from '@storybook/addon-a11y';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import Card from 'ui/Card';
import List from 'ui/List';

import characters from './Card.fixtures';

const onClick = action('onClick');
const onSelect = action('onSelect');

storiesOf('Components/Cards', module)
  .addDecorator(checkA11y)
  .addDecorator(withKnobs)
  .add('Card', () => {
    const inline = boolean('Inline', false);
    return (
      <Card
        style={{ maxWidth: 600, maxHeight: inline ? 120 : '100%' }}
        link="/"
        image={characters[0].image}
        title={characters[0].name}
        inline={inline}
        value={characters[0].name}
        onClick={onClick}
      />
    )
  })
  .add('Card List', () => {
    const listView = boolean('List View', false);
    console.log(listView);
    return (
      <List view={listView ? 'list' : 'grid'} onViewChange={onClick}>
        {characters.map(({ name, image }) => (<Card
          key={name}
          style={{ maxWidth: 600, maxHeight: listView ? 120 : '100%' }}
          link="/"
          image={image}
          title={name}
          inline={listView}
          value={name}
          onClick={onClick}
        />))}

      </List>
    );
  });
