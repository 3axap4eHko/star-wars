import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { checkA11y } from '@storybook/addon-a11y';
import { withKnobs, select, text } from '@storybook/addon-knobs';

import Button from 'ui/Button';

const onClick = action('onClick');
const onSelect = action('onSelect');

storiesOf('Components', module)
  .addDecorator(checkA11y)
  .addDecorator(withKnobs)
  .add('Buttons', () => (
    <div style={{ display: 'grid', gridTemplateColumns: '1fr', gridRowGap: 16, alignItems: 'center' }}>
      <Button type="contained">Contained</Button>
      <Button type="outlined">Outlined</Button>
      <Button type="text">Text</Button>
      <Button type="icon" icon="view-list" />
    </div>

  ));
