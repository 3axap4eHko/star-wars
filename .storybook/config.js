import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { setDefaults, withInfo } from '@storybook/addon-info';
import { withOptions } from '@storybook/addon-options';
import { withBackgrounds } from '@storybook/addon-backgrounds';
import Wrapper from './Wrapper';

function Decorator(storyFn) {
  return <Wrapper storyFn={storyFn} />;
}

addDecorator(Decorator);
addDecorator(withOptions({
  name: 'Star Wars Media Lib',
  url: 'https://star-wars.com',
}));
addDecorator(withInfo({
  source: false,
}));
addDecorator(withBackgrounds([
  { name: 'dark', value: '#222222', default: true },
  { name: 'light', value: '#f1f1f1' },
]));

// automatically import all files ending in *.stories.js
const req = require.context('../stories', true, /.stories.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
