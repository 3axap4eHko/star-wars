const Path = require('path');

module.exports = {
  optimization: {
    minimize: false,
  },
  resolve: {
    alias: {
      'ui': Path.resolve(__dirname, '../src/components'),
    },
  },
  module: {
    rules: [
      { test: /\.(svg|jpg|png|gif)$/, loader: 'file-loader', options: { name: `/images/[name].[hash].[ext]` } },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader',
        options: { name: `fonts/[name].[hash].[ext]`, limit: 5000, mimetype: 'application/font-woff' },
      },
      { test: /\.ttf$|\.eot$/, loader: 'file-loader', options: { name: `fonts/[name].[hash].[ext]` } },
      { test: /\.css$/, use: [{ loader: 'style-loader' }, { loader: 'css-loader', options: { sourceMap: true } }] },
    ],
  },
};
