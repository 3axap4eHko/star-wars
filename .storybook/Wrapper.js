import React, { Component as ReactComponent } from 'react';
import { HashRouter } from 'react-router-dom';
import injectSheet, { ThemeProvider } from 'react-jss';
import theme from '../theme';

const withStyles = injectSheet(({ colors }) => ({
  '@global': {
    '#root': {
      display: 'flex',
      width: '100%',
      minHeight: '100vh',
      '& > div': {
        flex: 1,
      }
    },
    a: {
      color: colors.text,
    },
    '*:focus': {
      outlineColor: 'rgba(0,0,0,0.4)',
      outlineStyle: 'dotted',
      outlineWidth: 1,
      outlineOffset: -1
    }
  },
  root: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

export default class Wrapper extends ReactComponent {
  render() {
    const { storyFn } = this.props;
    const Component = withStyles(storyFn);
    return (
      <HashRouter>
        <ThemeProvider theme={theme}>
          <Component />
        </ThemeProvider>
      </HashRouter>
    );
  }
}
