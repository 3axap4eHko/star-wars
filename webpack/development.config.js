const Path = require('path');
const { HotModuleReplacementPlugin, NamedModulesPlugin } = require('webpack');

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  devServer: {
    contentBase: './build',
    hot: true,
    quiet: false,
    port: 9090,
    stats: 'errors-only'
  },
  entry: {
    app: Path.resolve(__dirname, '../src/index.js')
  },
  plugins: [
    new NamedModulesPlugin(),
    new HotModuleReplacementPlugin(),
  ]
};
