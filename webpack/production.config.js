const Path = require('path');
const { HashedModuleIdsPlugin } = require('webpack');
//const Offline = require('offline-plugin');

module.exports = {
  mode: 'production',
  entry: {
    'index': Path.resolve(__dirname, '../src/index.js'),
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },
  plugins: [
    new HashedModuleIdsPlugin(),
  ],
};