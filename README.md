# Star Wars Media Lib

May the Force be with you

Application: https://star-wars-media-lib.herokuapp.com/

Storybook: https://star-wars-media-lib.herokuapp.com/storybook/

Coverage Report: https://star-wars-media-lib.herokuapp.com/coverage/clover.xml

Coverage Preview: https://star-wars-media-lib.herokuapp.com/coverage/lcov-report/

Repo: https://bitbucket.org/3axap4eHko/star-wars/

CI/CD: https://bitbucket.org/3axap4eHko/star-wars/addon/pipelines/home