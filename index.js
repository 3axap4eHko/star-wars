const express = require('express');
const path = require('path');
const PORT = process.env.PORT || 5000;

express()
  .use(express.static(path.join(__dirname, 'build')))
  .use('/coverage', express.static(path.join(__dirname, 'coverage')))
  .use((req, res) => {
    res.status(404).end();
  })
  .listen(PORT, () => console.log(`Listening on ${PORT}`));