module.exports = {
  verbose: true,
  'coverageDirectory': 'build/coverage',
  'moduleNameMapper': {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)$': '<rootDir>/src/__mocks__/fileMock.js',
    '\\.(css|less)$': '<rootDir>/src/__mocks__/styleMock.js',
  },
  'moduleFileExtensions': ['js', 'jsx', 'jss', 'json', 'jsxm', 'node'],
  'modulePathIgnorePatterns': [
    '<rootDir>/build',
  ],
  'unmockedModulePathPatterns': [
    'node_modules/react/',
    'node_modules/enzyme/',
  ],
  'testMatch': [
    '<rootDir>/src/**/__tests__/**/*.js',
  ],
  'setupFiles': [
    '<rootDir>/jest.setup.js'
  ],
};
