export const xs = 0;
export const sm = 600;
export const md = 1024;
export const lg = 1440;
export const xl = 1920;
