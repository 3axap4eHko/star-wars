import * as breakpoints from './breakpoints';
import screens from './screens';
import shadows from './shadows';
import * as transitions from './transitions';

function upperFirst(str) {
  return str[0].toUpperCase() + str.slice(1);
}

function genClass(result, ...parts) {
  for (let i = 0; i < parts.length; i++) {
    if (parts[i]) {
      result += upperFirst(parts[i].toString());
    }
  }
  return result;
}

const palette = {
  white: '#ffffff',
  black: '#000000',
  darkgrey: '#222222',
  orange: '#ffaa00',
  red: '#ff3333',
};

export default {
  palette,
  colors: {
    background: palette.darkgrey,
    text: palette.white,
    primary: palette.orange,
    secondary: palette.red,
  },
  shape: {
    borderRadius: 5,
  },

  breakpoints,
  screens,
  shadows,
  transitions,

  genClass,
};