import { xs, sm, md, lg, xl } from './breakpoints';

export default {
  xs: `@media (min-width: ${xs}px) and (max-width: ${sm - 1}px)`,
  sm: `@media (min-width: ${sm}px) and (max-width: ${md - 1}px)`,
  md: `@media (min-width: ${md}px) and (max-width: ${lg - 1}px)`,
  lg: `@media (min-width: ${lg}px) and (max-width: ${xl - 1}px)`,
  xl: `@media (min-width: ${xl}px)`,

  smUp: `@media (min-width: ${sm}px)`,
  mdUp: `@media (min-width: ${md}px)`,
  lgUp: `@media (min-width: ${lg}px)`,

  smDown: `@media (max-width: ${md - 1}px)`,
  mdDown: `@media (max-width: ${lg - 1}px)`,
  lgDown: `@media (max-width: ${xl - 1}px)`,

};