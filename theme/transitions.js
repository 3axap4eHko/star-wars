export const easing = {
  easeInOut: 'cubic-bezier(0.4, 0, 0.2, 1)',
  easeOut: 'cubic-bezier(0.0, 0, 0.2, 1)',
  easeIn: 'cubic-bezier(0.4, 0, 1, 1)',
  sharp: 'cubic-bezier(0.4, 0, 0.6, 1)',
};

export const duration = {
  shortest: 150,
  shorter: 200,
  short: 250,
  standard: 300,
  complex: 375,
  enteringScreen: 225,
  leavingScreen: 195,
};

export const formatMs = milliseconds => `${Math.round(milliseconds)}ms`;
export const isString = value => typeof value === 'string';
export const isNumber = value => !isNaN(parseFloat(value));

export function create(props = ['all'], options = {}) {
  const {
          duration: durationOption = duration.standard,
          easing: easingOption     = easing.easeInOut,
          delay                    = 0,
        } = options;

  return (Array.isArray(props) ? props : [props])
    .map(animatedProp =>
      `${animatedProp} ${isString(durationOption) ? durationOption : formatMs(durationOption)} ${easingOption} ${isString(delay) ? delay : formatMs(delay)}`,
    )
    .join(',');
}