import React, { Component, Fragment } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import asyncComponent from 'react-helpful/asyncComponent';
import Navigation from 'ui/Navigation';
import Footer from 'ui/Footer';
import Error, { CatchProvider } from 'ui/Error';
import styled from './App.styles';

const HomePage = asyncComponent(() => import('./pages/Home'));
const CharacterPage = asyncComponent(() => import('./pages/Character'));
const ErrorPage = asyncComponent(() => import('./pages/Error'));

@styled
export default class App extends Component {
  render() {
    return (
      <Fragment>
        <HashRouter>
          <Route
            render={({ location }) => (
              <Fragment>
                <Navigation />
                <TransitionGroup component={Fragment}>
                  <CSSTransition
                    key={location.key}
                    className="test"
                    classNames="fade"
                    timeout={300}
                  >
                    <CatchProvider location={location}>
                      {error => (
                        <Switch location={location}>
                          <Route path={error.pathname} component={ErrorPage} exact />
                          <Route exact path="/" component={HomePage} />
                          <Route exact path="/character/:character" component={CharacterPage} />
                          <Route render={() => <Error code={404} />} />
                        </Switch>
                      )}
                    </CatchProvider>
                  </CSSTransition>
                </TransitionGroup>
                <Footer />
              </Fragment>
            )}
          />
        </HashRouter>
      </Fragment>
    );
  }
}