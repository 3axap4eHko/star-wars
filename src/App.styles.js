import injectSheet from 'react-jss';

export default injectSheet(({ colors }) => ({
  '@global': {
    'html, body': {
      display: 'flex',
      justifyContent: 'center',
      margin: 0,
      minWidth: '100%',
      minHeight: '100%',
      background: colors.background,
    },
    '#app': {
      position: 'relative',
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
      maxWidth: 800,
      color: '#ffffff',
      paddingBottom: 64,
    },
    a: {
      color: colors.primary,
    },
    '*:focus': {
      outlineColor: 'rgba(0,0,0,0.4)',
      outlineStyle: 'dotted',
      outlineWidth: 1,
      outlineOffset: -1,
    },
  },
  root: {},
}));