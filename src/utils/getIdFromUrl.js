export default function (url) {
  const matches = url.match(/(\w+)\/?$/);
  if (matches) {
    return matches[1];
  }
  return 0;
}