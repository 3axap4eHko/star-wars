import React, { Component } from 'react';
import { string, oneOf } from 'prop-types';
import classNames from 'classnames';
import styled from './Button.styles';

@styled
export default class Button extends Component {
  static propTypes = {
    color: oneOf(['default', 'primary', 'secondary']),
    type: oneOf(['contained', 'outlined', 'text', 'icon']),
    icon: string,
  };
  static defaultProps = {
    type: 'contained',
  };

  render() {
    const { classes, className, children, color, type, icon, theme: { genClass }, ...props } = this.props;
    const _className = classNames(
      classes.root,
      classes[genClass('type', type)],
      classes[genClass('color', color)],
      icon && `mdi`,
      icon && `mdi-${icon}`,
      className,
    );

    return (
      <button className={_className} {...props}>
        {children}
      </button>
    );
  }
}
