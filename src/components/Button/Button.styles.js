import injectSheet from 'react-jss';

export default injectSheet(({ colors, shadows, shape }) => ({
  root: {
    border: 'none',
    height: 32,
    fontSize: 18,
    textTransform: 'uppercase',
    boxShadow: shadows.elevation2,
    borderRadius: shape.borderRadius,
    '&:hover': {
      boxShadow: shadows.elevation4,
    },
    '&:active': {
      boxShadow: shadows.elevation8,
    },
  },

  typeContained: {
    backgroundColor: colors.text,
    color: colors.background,
  },
  typeOutlined: {
    border: `2px solid ${colors.text}`,
    backgroundColor: colors.background,
    color: colors.text,
  },
  typeText: {
    backgroundColor: 'transparent',
    color: colors.text,
    boxShadow: 'none',
  },
  typeIcon: {
    color: 'inherit',
    backgroundColor: 'transparent',
    textTransform: 'none',
    borderRadius: '50%',
    boxShadow: 'none',
    maxWidth: 56,
    height: 56,
    fontSize: 32,
    '&:hover': {
      boxShadow: 'none',
      backgroundColor: '#414141'
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#414141'
    },

  },
}));