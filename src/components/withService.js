import React, { Component } from 'react';
import withAwait from 'react-helpful/withAwait';
import Error from 'ui/Error';
import nprogress from 'nprogress';

export default function (fetcher) {
  const withAwaitHoc = withAwait(props => () => fetcher(props));

  return WrappedComponent => {

    return withAwaitHoc(class WithService extends Component {
      render() {
        const { data: { result, loading, error }, ...props } = this.props;
        if (loading) {
          nprogress.start();
        } else {
          nprogress.done();
        }
        if (error) {
          return <Error code={404} stack={error.stack} />;
        }
        if (!result) {
          return <div>...Loading</div>;
        }
        return (
          <WrappedComponent {...props} data={result} />
        );
      }
    });
  };
}