import React, { Component, cloneElement, isValidElement } from 'react';
import { number, bool, func, oneOfType, shape } from 'prop-types';
import Transition from 'react-transition-group/Transition';
import { reflow, getTransitionProps } from '../../utils/transitions';

import styled from './Fade.styles';

const styles = {
  entering: {
    opacity: 1,
  },
  entered: {
    opacity: 1,
  },
};

@styled
export default class Fade extends Component {

  static propTypes = {
    in: bool,
    onEnter: func,
    onExit: func,
    timeout: oneOfType([
      number,
      shape({ enter: number, exit: number }),
    ]).isRequired,
  };

  handleEnter = node => {
    const { theme } = this.props;
    reflow(node);

    const transitionProps = getTransitionProps(this.props, {
      mode: 'enter',
    });
    node.style.webkitTransition = theme.transitions.create('opacity', transitionProps);
    node.style.transition = theme.transitions.create('opacity', transitionProps);

    if (this.props.onEnter) {
      this.props.onEnter(node);
    }
  };

  handleExit = node => {
    const { theme } = this.props;
    const transitionProps = getTransitionProps(this.props, {
      mode: 'exit',
    });
    node.style.webkitTransition = theme.transitions.create('opacity', transitionProps);
    node.style.transition = theme.transitions.create('opacity', transitionProps);

    if (this.props.onExit) {
      this.props.onExit(node);
    }
  };

  render() {
    const { classes, children, onEnter, onExit, style: styleProp, theme, ...other } = this.props;

    const style = {
      ...styleProp,
      ...(isValidElement(children) ? children.props.style : {}),
    };

    return (
      <Transition appear onEnter={this.handleEnter} onExit={this.handleExit} {...other}>
        {(state, childProps) => {
          return cloneElement(children, {
            style: {
              opacity: 0,
              willChange: 'opacity',
              ...styles[state],
              ...style,
            },
            ...childProps,
          });
        }}
      </Transition>
    );
  }
}
