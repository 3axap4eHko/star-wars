import React from 'react';
import Button from '../Button';
import renderer from 'react-test-renderer';
import theme from '../../../theme';
import { ThemeProvider } from 'react-jss';
import { mount } from 'enzyme';

function renderWithTheme(element) {
  return renderer.create(
    <ThemeProvider theme={theme}>
      {element}
    </ThemeProvider>,
  );
}

function mountWithTheme(element) {
  return mount(
    <ThemeProvider theme={theme}>
      {element}
    </ThemeProvider>,
  );
}

describe('Button suite', () => {

  test('snapshot', () => {
    const element = <Button type="contained">Contained</Button>;
    const tree = renderWithTheme(element).toJSON();

    expect(tree).toMatchSnapshot();
  });

  test('contained', () => {
    const element = <Button type="contained">Contained</Button>;
    const button = mountWithTheme(element);
    expect(button.text()).toEqual('Contained');
    expect(button.find('button[class^="Button-typeContained"]')).toBeTruthy();
  });

  test('outlined', () => {
    const element = <Button type="outlined">Outlined</Button>;
    const button = mountWithTheme(element);
    expect(button.text()).toEqual('Outlined');
    expect(button.find('button[class^="Button-typeOutlined"]')).toBeTruthy();
  });

  test('icon', () => {
    const element = <Button type="icon" icon="view-list" />;
    const button = mountWithTheme(element);
    expect(button.find('button[class^="Button-typeIcon"]')).toBeTruthy();
    expect(button.find('button[class^="mdi-view-list"]')).toBeTruthy();
  });

});