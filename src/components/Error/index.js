import React, { Component, createContext } from 'react';
import { number, func } from 'prop-types';

const Context = createContext({});

export class CatchProvider extends Component {
  static propTypes = {
    children: func,
  };

  state = { pathname: '/error', code: 0 };

  setError = (code, pathname, stack) => {
    console.error('ERROR', pathname, code, stack);
    pathname = pathname || this.props.location.pathname;
    this.setState({ code, pathname });
  };

  render() {
    const { children } = this.props;
    return (
      <Context.Provider value={{ error: this.state, setError: this.setError }}>
        {children(this.state)}
      </Context.Provider>
    );
  }
}

export function withCatch(WrappedComponent) {

  return function CatchConsumer(props) {
    return (
      <Context.Consumer>
        {context => <WrappedComponent {...props} {...context} />}
      </Context.Consumer>
    );
  };
}

@withCatch
export default class Error extends Component {
  static propTypes = {
    code: number,
  };

  static getDerivedStateFromProps({ code, stack, setError }) {
    setError(code, null, stack);

    return null;
  }

  state = {};

  render() {
    return null;
  }
}
