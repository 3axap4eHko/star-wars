import React, { Component } from 'react';
import { string, bool } from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import Image from '../Image';
import styled from './Card.styles';

@styled
export default class Card extends Component {
  static propTypes = {
    link: string,
    image: string,
    title: string,
    inline: bool,
  };

  handleClick = (event) => {
    const { onClick, value } = this.props;
    if (onClick) {
      onClick(event, value);
    }
  };

  render() {
    const { classes, className, style, link, image, title, inline, theme: { genClass } } = this.props;
    return (
      <Link to={link} className={classNames(classes.root, { [classes.inline]: inline })} style={style} onClick={this.handleClick}>
        {image && <Image
          className={classes.image}
          figureClassName={classNames(classes.figure, { [classes.figureInline]: inline })}
          src={image}
          overlay={!inline && !!title}
        />}
        {title && <h3 className={classNames(classes.title, { [classes.titleInline]: inline })}>{title}</h3>}
      </Link>
    );
  }
}
