import injectSheet from 'react-jss';

export default injectSheet(({ shape, shadows }) => ({
  root: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    maxWidth: '100%',
    borderRadius: shape.borderRadius,
    boxShadow: shadows.elevation4,
    overflow: 'hidden',
    '&:hover': {
      boxShadow: shadows.elevation12,
      '& $image': {
        maxWidth: '120%'
      },
    }
  },
  inline: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    transition: '250ms',
  },
  figure: {
    flex: 1,
    maxWidth: '100%',
    transition: 'all 250ms ease-in-out',
    minWidth: '20%',
  },
  figureInline: {
    maxWidth: '30%',
  },
  title: {
    position: 'absolute',
    padding: '0 24px',
    bottom: 0,
  },
  titleInline: {
    position: 'relative',
  },
}));