import React, { Component } from 'react';
import {} from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import styled from './Navigation.styles';

@styled
@withRouter
export default class Navigation extends Component {
  render() {
    const { classes, children } = this.props;
    return (
      <div className={classes.root}>
        <Link to="/"><h3 className={classes.title}>Star Wars Media Lib</h3></Link>
        {children}
      </div>
    );
  }
}
