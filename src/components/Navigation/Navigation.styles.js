import injectSheet from 'react-jss';

export default injectSheet(({ shadows }) => ({
  root: {
    display: 'flex',
    padding: '8px 24px',
    marginBottom: 16,
    boxShadow: shadows.elevation4,
  },
  title: {
    textTransform: 'uppercase',
  }
}));