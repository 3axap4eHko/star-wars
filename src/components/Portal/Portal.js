import React, { Component } from 'react';
import { findDOMNode, createPortal } from 'react-dom';
import { object, func, bool, node, oneOfType } from 'prop-types';
import ownerDocument from '../../utils/ownerDocument';

function getContainer(container, defaultContainer) {
  container = typeof container === 'function' ? container() : container;
  return findDOMNode(container) || defaultContainer;
}

function getOwnerDocument(element) {
  return ownerDocument(findDOMNode(element));
}

export default class Portal extends Component {
  static propTypes = {
    children: node.isRequired,
    container: oneOfType([object, func]),
    disablePortal: bool,
    onRendered: func,
  };

  static defaultProps = {
    disablePortal: false,
  };

  setMountNode = container => {
    if (this.props.disablePortal) {
      this.mountNode = findDOMNode(this).parentElement;
    } else {
      this.mountNode = getContainer(container, getOwnerDocument(this).body);
    }
  };

  getMountNode = () => {
    return this.mountNode;
  };

  componentDidMount() {
    this.setMountNode(this.props.container);

    // Only rerender if needed
    if (!this.props.disablePortal) {
      this.forceUpdate(this.props.onRendered);
    }
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.container !== this.props.container ||
      prevProps.disablePortal !== this.props.disablePortal
    ) {
      this.setMountNode(this.props.container);

      // Only rerender if needed
      if (!this.props.disablePortal) {
        this.forceUpdate(this.props.onRendered);
      }
    }
  }

  componentWillUnmount() {
    this.mountNode = null;
  }

  render() {
    const { children, disablePortal } = this.props;

    if (disablePortal) {
      return children;
    }

    return this.mountNode ? createPortal(children, this.mountNode) : null;
  }
}
