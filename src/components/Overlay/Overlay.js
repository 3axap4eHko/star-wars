import React, { Component } from 'react';
import { bool, func, object, oneOfType, number } from 'prop-types';
import classNames from 'classnames';
import EventListener from 'react-helpful/EventListener';
import Backdrop from '../Backdrop';
import Portal from '../Portal';

import styled from './Overlay.styles';

@styled
export default class Overlay extends Component {
  static propTypes = {
    open: bool,
    container: oneOfType([object, func]),
    transitionDuration: number.isRequired,
    onCloseRequest: func.isRequired,
    onBackdropClick: func,
  };

  handleKeyPress = ({ key }) => {
    if (key === 'Escape') {
      this.props.onCloseRequest();
    }
  };

  render() {
    const { classes, className, transitionDuration, container, open, onBackdropClick, children } = this.props;

    const _className = classNames(classes.root, className);

    return (
      <Portal
        container={container}
      >
        <Backdrop className={_className} transitionDuration={transitionDuration} open={open} onClick={onBackdropClick}>
          {children}
        </Backdrop>
        <EventListener
          target={() => document}
          event="keydown"
          on={this.handleKeyPress}
        />
      </Portal>
    );
  }
}
