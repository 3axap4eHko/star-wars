import injectSheet from 'react-jss';

export default injectSheet(({ screens, palette }) => ({
  root: {
    overflow: 'auto',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    zIndex: 2000,
    color: palette.white,
    textAlign: 'left',
    position: 'fixed',
    background: 'rgba(0,0,0,0.5)',
    transition: 'visibility 0s ease-out 0.2s,opacity 0.2s ease-out',
    '-webkit-overflow-scrolling': 'touch',
  },
  noScroll: {
    [screens.smUp]: {
      height: '100%',
      overflow: 'hidden',
    },
  },
}))
;
