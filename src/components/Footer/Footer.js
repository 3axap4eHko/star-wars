import React, { Component } from 'react';
import {} from 'prop-types';
import styled from './Footer.styles';

@styled
export default class Footer extends Component {
  render() {
    const { classes } = this.props;
    return (
      <h3 className={classes.root}>
        Copyright 2019 by Zakharchanka Ivan
      </h3>
    );
  }
}
