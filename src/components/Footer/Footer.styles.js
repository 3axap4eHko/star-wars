import injectSheet from 'react-jss';

export default injectSheet(theme => ({
  root: {
    position:'absolute',
    bottom: 0,
  },
}));