import injectSheet from 'react-jss';

export default injectSheet(({ colors, screens, shadows }) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    display: 'flex',
  },
  viewIcon: {
    cursor: 'pointer',
    margin: 4,
    fontSize: 42,
    '&:hover': {
      color: colors.primary,
    },
  },
  viewIconActive: {
    backgroundColor: '#414141',
  },
  body: {
    display: 'grid',
    gridTemplateColumns: '1fr',
    gridColumnGap: 24,
    gridRowGap: 16,
    [screens.smUp]: {
      gridTemplateColumns: '1fr 1fr',
      gridColumnGap: 24,
      gridRowGap: 16,
    },
    '& > *': {
      transition: 'all 250ms ease-in-out',
      maxHeight: '100vh',
      overflow: 'hidden',
    },
  },
  bodyList: {
    gridTemplateColumns: '1fr',
    '& > *': {
      maxHeight: 120,
    },
  },
}));