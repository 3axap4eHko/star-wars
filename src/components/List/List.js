import React, { Component } from 'react';
import { bool, oneOf, func } from 'prop-types';
import classNames from 'classnames';
import Button from '../Button';
import styled from './List.styles';

@styled
export default class List extends Component {
  static propTypes = {
    controls: bool,
    view: oneOf(['list', 'grid']),
    onViewChange: func,
  };

  static defaultProps = {
    controls: true,
    view: 'grid',
  };

  handleSetViewList = () => {
    const { onViewChange } = this.props;
    onViewChange('list');
  };

  handleSetViewGrid = () => {
    const { onViewChange } = this.props;
    onViewChange('grid');
  };

  render() {
    const { classes, header, children, controls, view, theme: { genClass } } = this.props;
    const _bodyClassName = classNames(
      classes.body,
      classes[genClass('body', view)],
    );

    return (
      <div className={classes.root}>
        {controls && <div className={classes.header}>
          <Button className={classNames(classes.viewIcon, { [classes.viewIconActive]: view === 'list' })} type="icon" icon="view-list" onClick={this.handleSetViewList} />
          <Button className={classNames(classes.viewIcon, { [classes.viewIconActive]: view === 'grid' })} type="icon" icon="view-module" onClick={this.handleSetViewGrid} />
          {header}
        </div>}
        <div className={_bodyClassName}>
          {children}
        </div>
      </div>
    );
  }
}
