import React, { Component, createRef } from 'react';
import { string, number, bool } from 'prop-types';
import classNames from 'classnames';
import styled from './Image.styles';

@styled
export default class Image extends Component {
  static propTypes = {
    src: string,
    srcSet: string,
    inline: string,
    alt: string,
    title: string,
    loadOffset: number,
    overlay: bool,
  };

  static defaultProps = {
    loadOffset: 100,
  };

  state = this.props.inline ? {
    src: this.props.inline,
    srcSet: '',
    extraStyle: { filter: 'blur(10px)' },
  } : {
    src: this.props.src,
    srcSet: this.props.srcSet,
    extraStyle: {},
  };

  ref = createRef();

  onScroll = () => {
    if (this.ref.current) {
      const y = this.ref.current.getBoundingClientRect().top - window.innerHeight;
      if (y <= this.props.loadOffset) {
        const { src, srcSet } = this.props;
        this.setState({
          src,
          srcSet,
          style: {},
        });
        this.unlisten();
      }
    }
  };

  componentDidMount() {
    this.unlisten = () => window.removeEventListener('scroll', this.onScroll);
    window.addEventListener('scroll', this.onScroll);

    this.onScroll();
  }

  componentWillUnmount() {
    this.unlisten();
  }

  render() {
    const { classes, className, figureClassName, pictureClassName, overlay, theme: { genClass }, loadOffset, ...props } = this.props;
    const { src, srcSet } = this.state;

    return (
      <figure className={classNames(classes.figure, figureClassName, overlay && classes.overlay)}>
        <picture className={classNames(classes.picture, pictureClassName)}>
          <img
            {...props}
            className={classNames(classes.image, className)}
            src={src}
            srcSet={srcSet}
            ref={this.ref}
          />
        </picture>
      </figure>
    );
  }
}
