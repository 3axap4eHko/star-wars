import injectSheet from 'react-jss';

export default injectSheet(theme => ({
  figure: {
    position: 'relative',
    paddingBottom: '56.25%',
    margin: 0,
    overflow: 'hidden',
  },
  overlay: {
    '&:after': {
      content: '""',
      position: 'absolute',
      backgroundColor: 'rgba(0,0,0,0.7)',
      left: 0,
      right: 0,
      top: '65%',
      bottom: 0,
    }
  },
  picture: {

  },
  image: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    maxWidth: '100%'
  }
}));