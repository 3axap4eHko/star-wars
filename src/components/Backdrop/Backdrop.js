import React, { Component } from 'react';
import { bool, number, oneOfType, shape } from 'prop-types';
import classNames from 'classnames';
import Fade from '../Fade';
import styled from './Backdrop.styles';

@styled
export default class Backdrop extends Component {
  static propTypes = {
    invisible: bool,
    open: bool.isRequired,
    transitionDuration: oneOfType([
      number,
      shape({ enter: number, exit: number }),
    ]),
  };

  render() {
    const { classes, className, invisible, open, transitionDuration, children, ...props } = this.props;

    const _className = classNames(classes.root, { [classes.invisible]: invisible }, className);

    return (
      <Fade in={open} timeout={transitionDuration} {...props}>
        <div
          className={_className}
          aria-hidden="true"
        >
          {children}
        </div>
      </Fade>
    );
  }
}
