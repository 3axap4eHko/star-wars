import injectSheet from 'react-jss';

export default injectSheet(theme => ({
  root: {
    zIndex: -1,
    position: 'fixed',
    right: 0,
    bottom: 0,
    top: 0,
    left: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    WebkitTapHighlightColor: 'transparent',
    touchAction: 'none',
  },
  invisible: {
    backgroundColor: 'transparent',
  },
}));