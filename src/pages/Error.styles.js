import injectSheet from 'react-jss';

export default injectSheet(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    minWidth: '100%',
  },
  title: {
    textTransform: 'uppercase',
  },
}));