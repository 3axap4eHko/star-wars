import React, { Component } from 'react';
import {} from 'prop-types';
import Card from 'ui/Card';
import withService from 'ui/withService';
import CharacterFilms from '../services/CharacterFilms';
import { characters } from '../characters.json';

import styled from './Character.styles';

@styled
@withService(({ match }) => {
  const id = match.params?.character;
  const character = characters.find(character => character.url.endsWith(`/${id}/`));
  if (!character) {
    throw new Error(`Character with id "${id}" not found`);
  }
  return {
    character,
  };
})
export default class Character extends Component {
  render() {
    const { classes, location, match, data: { character: { name, url } } } = this.props;

    return (
      <div>
        <Card
          link={location.pathname}
          image={`/images/character-${match.params?.character}.png`}
          title={name}
        />
        <h3 className={classes.title}>Films</h3>
        <CharacterFilms location={location} url={url} />
      </div>
    );
  }
}
