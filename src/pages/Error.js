import React, { Component, Fragment } from 'react';
import {} from 'prop-types';
import { withCatch } from 'ui/Error';
import Image from 'ui/Image';
import styled from './Error.styles';

@styled
@withCatch
export default class Error extends Component {
  render() {
    const { classes, error, children } = this.props;

    return (
      <div className={classes.root}>
        <Fragment>
          <Image figureClassName={classes.image} src={'/images/not-found.png'} />
          <h3 className={classes.title}>To the hand you talk, because listening I'm not</h3>
        </Fragment>
      </div>
    );
  }
}
