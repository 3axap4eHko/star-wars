import React, { Component, Fragment } from 'react';
import List from 'ui/List';
import Card from 'ui/Card';
import getIdFromUrl from '../utils/getIdFromUrl';
import { characters } from '../characters.json';

import styled from './Home.styles';

const STORAGE_VIEW = 'characters_view';

@styled
export default class Home extends Component {

  state = {
    view: localStorage.getItem(STORAGE_VIEW) || 'grid',
  };

  handleViewChange = view => {
    localStorage.setItem(STORAGE_VIEW, view);
    this.setState({ view });
  };

  render() {
    const { classes, children } = this.props;
    const { view } = this.state;

    return (
      <Fragment>
        <List view={view} onViewChange={this.handleViewChange}>
          {characters.map(({ name, url }, idx) => {
            const id = getIdFromUrl(url);

            return <Card
              key={url}
              link={`/character/${id}`}
              image={`/images/character-${id}.png`}
              title={name}
              inline={view === 'list'}
            />;
          })}
        </List>
      </Fragment>
    );
  }
}
