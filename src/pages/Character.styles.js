import injectSheet from 'react-jss';

export default injectSheet(theme => ({
  root: {},
  title: {
    textAlign: 'center',
    textTransform: 'uppercase',
  }
}));