import React, { Component } from 'react';
import {} from 'prop-types';
import classNames from 'classnames';
import Card from 'ui/Card';
import Overlay from 'ui/Overlay';
import getIdFromUrl from '../utils/getIdFromUrl';
import styled from './FilmInfo.styles';

@styled
export default class FilmInfo extends Component {

  handleCardClick = (event) => {
    event.preventDefault();
  };

  render() {
    const { classes, className, film, onCloseRequest } = this.props;

    //title,episode_id,opening_crawl,director,producer,release_date

    return (
      <Overlay
        open={true}
        className={classNames(classes.overlay, className)}
        transitionDuration={250}
        onCloseRequest={onCloseRequest}
        onBackdropClick={onCloseRequest}
      >
        <div className={classes.body}>
          <Card
            link={`/movie/${getIdFromUrl(film.url)}`}
            image={'/images/placeholder.png'}
            onClick={this.handleCardClick}
          />
          <h2>{film.title}</h2>
          <h3>Episode {film.episode_id}</h3>
          <p>{film.opening_crawl}</p>
        </div>
      </Overlay>
    );
  }
}
