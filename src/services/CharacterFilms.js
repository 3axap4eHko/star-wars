import React, { Component, Fragment } from 'react';
import {} from 'prop-types';
import List from 'ui/List';
import Card from 'ui/Card';
import withService from 'ui/withService';
import getIdFromUrl from '../utils/getIdFromUrl';
import FilmInfo from './FilmInfo';

const STORAGE_VIEW = 'films_view';
const STORAGE_ORDER = 'films_order';

@withService(async props => {
  const character = await (await fetch(props.url)).json();
  const films = await Promise.all(character.films.map(async filmUrl => (await fetch(filmUrl)).json()));
  return {
    character,
    films,
  };
})
export default class CharacterFilms extends Component {

  state = {
    view: localStorage.getItem(STORAGE_VIEW) || 'grid',
    order: parseInt(localStorage.getItem(STORAGE_ORDER)) || 1,
    open: false,
    film: null,
  };

  handleViewChange = view => {
    localStorage.setItem(STORAGE_VIEW, view);
    this.setState({ view });
  };

  handleOrderChange = ({ target: { value } }) => {
    localStorage.setItem(STORAGE_ORDER, value);
    this.setState({ order: parseInt(value) });
  };

  handleCardClick = async (event, film) => {
    event.preventDefault();
    this.setState({ open: true, film });
  };

  handleClose = () => {
    this.setState({ open: false, film: null });
  };

  render() {
    const { location, data: { films } } = this.props;
    const { view, order, open, film } = this.state;

    return (
      <Fragment>
        <List
          view={view}
          header={(
            <Fragment>
              <select value={order} style={{ backgroundColor: 'transparent', color: 'inherit', height: 32, margin: '15px 10px' }} onChange={this.handleOrderChange}>
                <option value={1}>ASC</option>
                <option value={-1}>DESC</option>
              </select>
            </Fragment>
          )}
          onViewChange={this.handleViewChange}
        >
          {films
            .sort((a, b) => order * (new Date(a.release_date) - new Date(b.release_date)))
            .map(film => <Card
              key={film.url}
              value={film}
              link={`/movie/${getIdFromUrl(film.url)}`}
              title={`${film.title} (${film.release_date}) by ${film.director}`}
              image={'/images/placeholder.png'}
              inline={view === 'list'}
              onClick={this.handleCardClick}
            />)}
        </List>
        {open && <FilmInfo open={open} film={film} onCloseRequest={this.handleClose} />}
      </Fragment>

    );
  }
}
