import injectSheet from 'react-jss';

export default injectSheet(({ colors, shape }) => ({
  '@global': {
    body: {
      overflow: 'hidden',
    }
  },
  overlay: {
    position: 'fixed',
  },
  body: {
    position: 'absolute',
    top: 0,
    left: '50%',
    transform: 'translateX(-50%)',
    width: '100%',
    maxWidth: 600,
    marginTop: 80,
    backgroundColor: colors.background,
    padding: 24,
    borderRadius: shape.borderRadius,
  },
}));