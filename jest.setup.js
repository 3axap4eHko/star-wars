import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import { JssProvider, ThemeProvider } from 'react-jss';
import { shallow, render, mount } from 'enzyme';
import theme from './theme';

Enzyme.configure({ adapter: new Adapter() });

